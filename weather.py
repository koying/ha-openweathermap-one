"""Support for the OpenWeatherMap (OWM) service."""
from datetime import timedelta
import logging

import voluptuous as vol
import homeassistant.helpers.config_validation as cv

from .const import (
    DOMAIN, 
    ATTRIBUTION, 
    FORECAST_MODE,
)

from homeassistant.components.weather import (
    ATTR_FORECAST_CONDITION,
    ATTR_FORECAST_PRECIPITATION,
    ATTR_FORECAST_TEMP,
    ATTR_FORECAST_TEMP_LOW,
    ATTR_FORECAST_TIME,
    ATTR_FORECAST_WIND_BEARING,
    ATTR_FORECAST_WIND_SPEED,
    WeatherEntity,
)
from homeassistant.const import (
    CONF_API_KEY,
    CONF_LATITUDE,
    CONF_LONGITUDE,
    CONF_MODE,
    CONF_NAME,
    PRESSURE_HPA,
    PRESSURE_INHG,
    STATE_UNKNOWN,
    TEMP_CELSIUS,
)
from homeassistant.util.pressure import convert as convert_pressure

_LOGGER = logging.getLogger(__name__)

CONDITION_CLASSES = {
    "cloudy": [803, 804],
    "fog": [701, 741],
    "hail": [906],
    "lightning": [210, 211, 212, 221],
    "lightning-rainy": [200, 201, 202, 230, 231, 232],
    "partlycloudy": [801, 802],
    "pouring": [504, 314, 502, 503, 522],
    "rainy": [300, 301, 302, 310, 311, 312, 313, 500, 501, 520, 521],
    "snowy": [600, 601, 602, 611, 612, 620, 621, 622],
    "snowy-rainy": [511, 615, 616],
    "sunny": [800],
    "windy": [905, 951, 952, 953, 954, 955, 956, 957],
    "windy-variant": [958, 959, 960, 961],
    "exceptional": [711, 721, 731, 751, 761, 762, 771, 900, 901, 962, 903, 904],
}

async def async_setup_platform(hass, config, async_add_entities, discovery_info=None):
    """Set up a owm weather entity."""
    _LOGGER.info("Initializing Openweathermap-one weather platform")

    if discovery_info is None:
        return
    
    hass.data[DOMAIN]["entities"]["weather"] = []
    for mode in FORECAST_MODE:
        async_add_entities([OpenWeatherMapWeather(*discovery_info, hass.config.units.temperature_unit, mode)])


class OpenWeatherMapWeather(WeatherEntity):
    """Implementation of an OpenWeatherMap sensor."""

    def __init__(self, api, int_name, temperature_unit, mode):
        """Initialize the sensor."""
        self.coordinator = api
        self._name = int_name
        self._temperature_unit = temperature_unit
        self._mode = mode

    @property
    def name(self):
        """Return the name of the sensor."""
        return f"{self._name} {self._mode}"

    @property
    def should_poll(self):
        """No need to poll. Coordinator notifies entity of updates."""
        return False

    @property
    def available(self):
        """Return if entity is available."""
        return self.coordinator.last_update_success


    @property
    def condition(self):
        """Return the current condition."""
        try:
            return [
                k
                for k, v in CONDITION_CLASSES.items()
                if self.coordinator.data.current.weather_code in v
            ][0]
        except IndexError:
            return STATE_UNKNOWN

    @property
    def temperature(self):
        """Return the temperature."""
        return self.coordinator.data.current.temperature("celsius").get("temp")

    @property
    def temperature_unit(self):
        """Return the unit of measurement."""
        return TEMP_CELSIUS

    @property
    def pressure(self):
        """Return the pressure."""
        pressure = self.coordinator.data.current.pressure.get("press")
        if self.hass.config.units.name == "imperial":
            return round(convert_pressure(pressure, PRESSURE_HPA, PRESSURE_INHG), 2)
        return pressure

    @property
    def humidity(self):
        """Return the humidity."""
        return self.coordinator.data.current.humidity

    @property
    def wind_speed(self):
        """Return the wind speed."""
        if self.hass.config.units.name == "imperial":
            return round(self.coordinator.data.current.wind().get("speed") * 2.24, 2)

        return round(self.coordinator.data.current.wind().get("speed") * 3.6, 2)

    @property
    def wind_bearing(self):
        """Return the wind bearing."""
        return self.coordinator.data.current.wind().get("deg")

    @property
    def attribution(self):
        """Return the attribution."""
        return ATTRIBUTION

    @property
    def forecast(self):
        """Return the forecast array."""
        data = []

        def calc_precipitation(rain, snow):
            """Calculate the precipitation."""
            rain_value = 0 if rain is None else rain
            snow_value = 0 if snow is None else snow
            if round(rain_value + snow_value, 1) == 0:
                return None
            return round(rain_value + snow_value, 1)

        if self._mode == "daily":
            weather = self.coordinator.data.forecast_daily
        else:
            weather = self.coordinator.data.forecast_hourly

        for entry in weather:
            if self._mode == "daily":
                data.append(
                    {
                        ATTR_FORECAST_TIME: entry.reference_time("unix") * 1000,
                        ATTR_FORECAST_TEMP: entry.temperature("celsius").get("max"),
                        ATTR_FORECAST_TEMP_LOW: entry.temperature("celsius").get(
                            "min"
                        ),
                        ATTR_FORECAST_PRECIPITATION: calc_precipitation(
                            entry.rain.get("all"), entry.snow.get("all")
                        ),
                        ATTR_FORECAST_WIND_SPEED: entry.wind().get("speed"),
                        ATTR_FORECAST_WIND_BEARING: entry.wind().get("deg"),
                        ATTR_FORECAST_CONDITION: [
                            k
                            for k, v in CONDITION_CLASSES.items()
                            if entry.weather_code in v
                        ][0],
                    }
                )
            else:
                data.append(
                    {
                        ATTR_FORECAST_TIME: entry.reference_time("unix") * 1000,
                        ATTR_FORECAST_TEMP: entry.temperature("celsius").get(
                            "temp"
                        ),
                        ATTR_FORECAST_PRECIPITATION: (
                            round(entry.rain.get("1h"), 1)
                            if entry.rain.get("1h") is not None
                            and (round(entry.rain.get("1h"), 1) > 0)
                            else None
                        ),
                        ATTR_FORECAST_CONDITION: [
                            k
                            for k, v in CONDITION_CLASSES.items()
                            if entry.weather_code in v
                        ][0],
                    }
                )
        return data

    async def async_added_to_hass(self):
        self.hass.data[DOMAIN]["entities"]["weather"].append(self)
        """Connect to dispatcher listening for entity data notifications."""
        self.coordinator.async_add_listener(self.async_write_ha_state)

    async def async_will_remove_from_hass(self):
        self.hass.data[DOMAIN]["entities"]["weather"].remove(self)
        """Disconnect from update signal."""
        self.coordinator.async_remove_listener(self.async_write_ha_state)
