from datetime import timedelta

DOMAIN = "openweathermap_one"
SCAN_INTERVAL = timedelta(minutes=10)

ATTRIBUTION = "Data provided by OpenWeatherMap (one call)"
FORECAST_MODE = ["hourly", "daily"]
DEFAULT_NAME = "OWM-one"

CONF_FORECAST = "forecast"
CONF_LANGUAGE = "language"

from homeassistant.const import (
    DEGREE,
    SPEED_METERS_PER_SECOND,
    PERCENTAGE,
)

SENSOR_TYPES = {
    "weather": ["Condition", None],
    "temperature": ["Temperature", None],
    "wind_speed": ["Wind speed", SPEED_METERS_PER_SECOND],
    "wind_bearing": ["Wind bearing", DEGREE],
    "humidity": ["Humidity", PERCENTAGE],
    "pressure": ["Pressure", "mbar"],
    "clouds": ["Cloud coverage", PERCENTAGE],
    "rain": ["Rain", "mm"],
    "snow": ["Snow", "mm"],
    "weather_code": ["Weather code", None],
}
