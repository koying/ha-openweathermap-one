"""Support for the OpenWeatherMap (OWM) service."""
from datetime import timedelta
import logging

import voluptuous as vol
import homeassistant.helpers.config_validation as cv

from homeassistant.helpers.entity import Entity
from homeassistant.components.sensor import (
    PLATFORM_SCHEMA
)

_LOGGER = logging.getLogger(__name__)

from .const import (
    DOMAIN,
    ATTRIBUTION, 
    CONF_FORECAST,
    DEFAULT_NAME,
    SENSOR_TYPES
)
from homeassistant.const import (
    ATTR_ATTRIBUTION,
    CONF_MONITORED_CONDITIONS,
    TEMP_CELSIUS,
    TEMP_FAHRENHEIT,
)

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend(
    {
        vol.Optional(CONF_MONITORED_CONDITIONS, default=[]): vol.All(
            cv.ensure_list, [vol.In(SENSOR_TYPES)]
        )
    }
)

async def async_setup_platform(hass, config, async_add_entities, discovery_info=None):
    """Set up a owm weather entity."""
    if discovery_info is None:
        return
    
    if discovery_info[0].config[CONF_MONITORED_CONDITIONS] is None:
        return

    _LOGGER.info("Initializing Openweathermap-one sensor platform (%s)", discovery_info[0].config)

    SENSOR_TYPES["temperature"][1] = hass.config.units.temperature_unit

    hass.data[DOMAIN]["entities"]["sensor"] = []
    dev = []
    for condition in discovery_info[0].config[CONF_MONITORED_CONDITIONS]:
        dev.append(
            OpenWeatherMapSensor(*discovery_info, condition, SENSOR_TYPES[condition][1])
        )
   
    async_add_entities(dev, True)


class OpenWeatherMapSensor(Entity):
    """Implementation of an OpenWeatherMap sensor."""

    def __init__(self, api, int_name, sensor_type, temp_unit):
        """Initialize the sensor."""
        self.coordinator = api
        self._int_name = int_name

        self._name = SENSOR_TYPES[sensor_type][0]
        self.temp_unit = temp_unit
        self.type = sensor_type
        self._unit_of_measurement = SENSOR_TYPES[sensor_type][1]

    @property
    def name(self):
        """Return the name of the sensor."""
        return f"{self._int_name} {self._name}"

    @property
    def state(self):
        """Return the state of the device."""
        try:
            if self.type == "weather":
                return self.coordinator.get_detailed_status()
            elif self.type == "temperature":
                if self.temp_unit == TEMP_CELSIUS:
                    return round(self.coordinator.data.current.temperature("celsius")["temp"], 1)
                elif self.temp_unit == TEMP_FAHRENHEIT:
                    return round(self.coordinator.data.current.temperature("fahrenheit")["temp"], 1)
                else:
                    return round(self.coordinator.data.current.temperature()["temp"], 1)
            elif self.type == "wind_speed":
                return round(self.coordinator.data.current.wind()["speed"], 1)
            elif self.type == "wind_bearing":
                return round(self.coordinator.data.current.wind()["deg"], 1)
            elif self.type == "humidity":
                return round(self.coordinator.data.current.humidity, 1)
            elif self.type == "pressure":
                return round(self.coordinator.data.current.pressure.get("press"), 0)
            elif self.type == "clouds":
                return self.coordinator.data.current.clouds
            elif self.type == "rain":
                rain = self.coordinator.data.current.rain
                if "1h" in rain:
                    self._unit_of_measurement = "mm"
                    return round(rain.get("1h"), 0)
                else:
                    self._unit_of_measurement = ""
                    return "not raining"
            elif self.type == "snow":
                if self.coordinator.data.current.snow:
                    self._unit_of_measurement = "mm"
                    return round(self.coordinator.data.current.get("all"), 0)
                else:
                    self._unit_of_measurement = ""
                    return "not snowing"
            elif self.type == "weather_code":
                return self.coordinator.data.current.weather_code
        except KeyError:
            _LOGGER.warning("Condition is currently not available: %s", self.type)
            return None

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement of this entity, if any."""
        return self._unit_of_measurement

    @property
    def device_state_attributes(self):
        """Return the state attributes."""
        return {ATTR_ATTRIBUTION: ATTRIBUTION}

    async def async_added_to_hass(self):
        self.hass.data[DOMAIN]["entities"]["sensor"].append(self)
        """Connect to dispatcher listening for entity data notifications."""
        self.coordinator.async_add_listener(self.async_write_ha_state)

    async def async_will_remove_from_hass(self):
        self.hass.data[DOMAIN]["entities"]["sensor"].remove(self)
        """Disconnect from update signal."""
        self.coordinator.async_remove_listener(self.async_write_ha_state)
