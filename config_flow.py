import logging
import voluptuous as vol
import homeassistant.helpers.config_validation as cv

from homeassistant import config_entries, core
from homeassistant.core import callback

from homeassistant.const import (
    CONF_API_KEY,
    CONF_LATITUDE,
    CONF_LONGITUDE,
    CONF_NAME,
    CONF_MONITORED_CONDITIONS,
)

from .const import (
    DEFAULT_NAME,
    CONF_LANGUAGE,
    SENSOR_TYPES,
)

from pyowm import OWM
from pyowm.commons import exceptions
from pyowm.utils.config import get_default_config

from .const import DOMAIN

_LOGGER = logging.getLogger(__name__)

async def validate_input(hass, data, longitude, latitude):
    owm = OWM(data[CONF_API_KEY])
    owmmgr = owm.weather_manager()

    obs = await hass.async_add_executor_job(owmmgr.one_call, latitude, longitude)
    if obs is None:
        _LOGGER.warning("Failed to fetch data from OWM")
        raise exceptions.ParseAPIResponseError

    info = {"name": data[CONF_NAME]}
    _LOGGER.debug("Setup ok with info: %s", info)
    return info

@config_entries.HANDLERS.register(DOMAIN)
class OWMConfigFlow(config_entries.ConfigFlow, domain=DOMAIN):
    """OWM config flow."""
    _LOGGER.debug("OWMConfigFlow")

    VERSION = 1
    CONNECTION_CLASS = config_entries.CONN_CLASS_CLOUD_POLL

    @staticmethod
    @callback
    def async_get_options_flow(config_entry):
        """OWM options callback."""
        return OWMOptionsFlowHandler(config_entry)

    async def async_step_user(self, user_input=None):
        """Handle the initial step."""
        _LOGGER.debug("owm async_step_user (%s)", user_input)

        errors = {}
        if user_input is not None:
            try:
                longitude = user_input.get(CONF_LONGITUDE, round(self.hass.config.longitude, 5))
                latitude = user_input.get(CONF_LATITUDE, round(self.hass.config.latitude, 5))
                info = await validate_input(self.hass, user_input, longitude, latitude)
            except exceptions.ParseAPIResponseError:
                errors["base"] = "cannot_connect"
            except Exception:  # pylint: disable=broad-except
                _LOGGER.exception("Unexpected exception")
                errors["base"] = "unknown"

            if "base" not in errors:
                await self.async_set_unique_id(info["name"])
                self._abort_if_unique_id_configured()
                title = f"{info['name']}"
                return self.async_create_entry(title=title, data=user_input)

        DATA_SCHEMA_CONFIG = vol.Schema({
                vol.Optional(CONF_NAME, default=DEFAULT_NAME): str,
                vol.Required(CONF_API_KEY): str,
                vol.Optional(CONF_LATITUDE, default=self.hass.config.latitude): cv.latitude,
                vol.Optional(CONF_LONGITUDE, default=self.hass.config.longitude): cv.longitude,
                vol.Optional(CONF_MONITORED_CONDITIONS, default=["temperature", "weather_code"]): vol.All(
                    cv.multi_select(
                        {
                            key: condition[0]
                            for key, condition in SENSOR_TYPES.items()
                        }
                    ))
                })

        return self.async_show_form(
            step_id="user", data_schema=DATA_SCHEMA_CONFIG, errors=errors
        )

    async def async_step_import(self, user_input):
        """Handle import."""
        _LOGGER.debug("owm async_step_import")
        return await self.async_step_user(user_input)

class OWMOptionsFlowHandler(config_entries.OptionsFlow):

    def __init__(self, config_entry):
        """Initialize OWM options flow."""
        _LOGGER.info("OWMOptionsFlowHandler : %s", config_entry.as_dict())
        self.api = None
        self.config_entry = config_entry
        self.name = config_entry.data[CONF_NAME]
        self.api_key = config_entry.data[CONF_API_KEY]
        self.monitored_conditions = config_entry.data[CONF_MONITORED_CONDITIONS] if CONF_MONITORED_CONDITIONS in config_entry.data else []
    
    async def async_step_init(self, user_input=None):
        """Manage the options."""
        self.api = self.hass.data[DOMAIN][self.name]
        self.longitude = self.config_entry.data[CONF_LONGITUDE] if CONF_LONGITUDE in self.config_entry.data else round(self.hass.config.longitude, 5)
        self.latitude = self.config_entry.data[CONF_LATITUDE] if CONF_LATITUDE in self.config_entry.data else round(self.hass.config.latitude, 5)
        return await self.async_step_user()

    async def async_step_user(self, user_input=None):
        """Handle a flow initialized by the user."""
        if user_input is not None:
            return self.async_create_entry(title="", data=user_input)

        DATA_SCHEMA_OPTINS = vol.Schema({
                vol.Required(CONF_API_KEY, default=self.api_key): str,
                vol.Optional(CONF_LATITUDE, default=self.latitude): cv.latitude,
                vol.Optional(CONF_LONGITUDE, default=self.longitude): cv.longitude,
                vol.Optional(CONF_MONITORED_CONDITIONS, default=self.monitored_conditions): vol.All(
                    cv.multi_select(
                        {
                            key: condition[0]
                            for key, condition in SENSOR_TYPES.items()
                        }
                    ))
                })

        return self.async_show_form(
            step_id="user", data_schema=DATA_SCHEMA_OPTINS
        )
