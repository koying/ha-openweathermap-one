"""The openweathermap component."""
import json
import logging
import asyncio
import datetime

from urllib import parse
from functools import partial
from pprint import pformat

from pyowm import OWM
from pyowm.commons import exceptions
from pyowm.utils.config import get_default_config

import voluptuous as vol

from homeassistant.core import callback

import homeassistant.helpers.config_validation as cv
from homeassistant.helpers import discovery
from homeassistant.helpers.update_coordinator import DataUpdateCoordinator, UpdateFailed
from homeassistant.helpers.dispatcher import (
    async_dispatcher_send,
)
from homeassistant.helpers.entity import Entity
from homeassistant.const import (
    CONF_API_KEY,
    CONF_LATITUDE,
    CONF_LONGITUDE,
    CONF_NAME,
    CONF_MONITORED_CONDITIONS,
)
from homeassistant.exceptions import ConfigEntryNotReady
from homeassistant.config_entries import SOURCE_IMPORT, ConfigEntry

from .const import (
    DOMAIN, 
    ATTRIBUTION, 
    FORECAST_MODE,
    SCAN_INTERVAL,
    DEFAULT_NAME,
    CONF_LANGUAGE,
    SENSOR_TYPES
)


__version__ = '0.0.1'

PLATFORMS = ["weather", "sensor"]

LOCATION_CONFIG_SCHEMA = vol.Schema({
          vol.Required(CONF_API_KEY): cv.string,
          vol.Optional(CONF_NAME, default=DEFAULT_NAME): cv.string,
          vol.Optional(CONF_LATITUDE): cv.latitude,
          vol.Optional(CONF_LONGITUDE): cv.longitude,
          vol.Optional(CONF_LANGUAGE): cv.string,
          vol.Optional(CONF_MONITORED_CONDITIONS, default=["temperature", "weather_code"]): vol.All(
            cv.ensure_list, [vol.In(SENSOR_TYPES)]
        )
       })

CONFIG_SCHEMA = vol.Schema({
    DOMAIN: vol.All(cv.ensure_list, [LOCATION_CONFIG_SCHEMA])
}, extra=vol.ALLOW_EXTRA)

SERVICE_SET_LOCATION = 'set_location'

SERVICE_SCHEMA = vol.Schema({
    vol.Required(CONF_NAME): cv.string,
})

SERVICE_SCHEMA_SET_LOCATION = SERVICE_SCHEMA.extend({
    vol.Required(CONF_LATITUDE): cv.latitude,
    vol.Required(CONF_LONGITUDE): cv.longitude
})

SERVICE_TO_METHOD = {
    SERVICE_SET_LOCATION: {'method': 'set_location', 'schema': SERVICE_SCHEMA_SET_LOCATION},
}

_LOGGER = logging.getLogger(__name__)

async def async_setup(hass, config):
    _LOGGER.info("Initializing Openweathermap_one integration")

    if not config[DOMAIN]:
        _LOGGER.info("Openweathermap_one config not found")
        return False

    for p_config in config[DOMAIN]:
        _LOGGER.info("Initializing Openweathermap_one integration (%s)", p_config)
        hass.async_create_task(hass.config_entries.flow.async_init(
            DOMAIN, context={"source": SOURCE_IMPORT}, data=p_config
        ))        
    
    return True

async def async_setup_entry(hass, conf_entry):
    """Configure the base device for Home Assistant."""
    _LOGGER.info("Initializing Openweathermap_one entry (%s)", conf_entry.as_dict())

    weather_data = WeatherData(hass, conf_entry.title, conf_entry.data)
    await weather_data.async_refresh()

    _LOGGER.info("refresh done")

    if not weather_data.last_update_success:
        raise ConfigEntryNotReady

    update_data = partial(
        async_update_items,
        weather_data,
        {}
    )

    weather_data.async_add_listener(update_data)
    update_data()

    hass.data.setdefault(DOMAIN, {})
    hass.data[DOMAIN][conf_entry.title] = weather_data
    hass.data[DOMAIN]["entities"] = {}

    async def async_service_handler(service):
        """Map services to methods"""
        api_client = hass.data[DOMAIN][service.data[CONF_NAME]]
        if api_client is None:
            _LOGGER.error("OpenWeatherMap instance is not found")
            return

        method = SERVICE_TO_METHOD.get(service.service)
        params = {key: value for key, value in service.data.items()
                  if key != CONF_NAME}

        if not hasattr(api_client, method['method']):
            _LOGGER.error("OpenWeatherMap instance has no method: %s", method['method'])
            return
        getattr(api_client, method['method'])(**params)

    for my_service in SERVICE_TO_METHOD:
        schema = SERVICE_TO_METHOD[my_service].get('schema', SERVICE_SCHEMA)
        hass.services.async_register(
            DOMAIN, my_service, async_service_handler, schema=schema)

    return True

async def async_unload_entry(hass, conf_entry):
    """Unload a config entry."""
    unload_ok = all(
        await asyncio.gather(
            *[
                hass.config_entries.async_forward_entry_unload(conf_entry, component)
                for component in PLATFORMS
            ]
        )
    )
    if unload_ok:
        hass.data[DOMAIN].pop(conf_entry.entry_id)

    return unload_ok

@callback
def async_update_items(api, current):
    """Update items."""
    _LOGGER.debug("Openweathermap_one: async_update_items")

    if (api.int_name in current):
      return

    current[api.int_name] = api.int_name

    for component in PLATFORMS:
      api.hass.async_create_task(
          discovery.async_load_platform(
              api.hass,
              component,
              DOMAIN,
              (api, api.int_name,),
              api.config
          )
      ) 

class WeatherData(DataUpdateCoordinator):
    """Get the latest data from OpenWeatherMap."""

    def __init__(self, hass, title, config):
        """Initialize the data object."""
        _LOGGER.debug("WeatherData ctor (%s)", config)
        self.hass = hass
        self.config = config
        self.int_name = title
        self.last_updated = datetime.datetime.fromtimestamp(0)
        self.data = {}

        self.longitude = self.config.get(CONF_LONGITUDE, round(hass.config.longitude, 5))
        self.latitude = self.config.get(CONF_LATITUDE, round(hass.config.latitude, 5))

        try:
            owm = OWM(self.config.get(CONF_API_KEY))
            self.owmmgr = owm.weather_manager()
        except exceptions.ParseAPIResponseError:
            _LOGGER.error("Error while connecting to OpenWeatherMap")

        super().__init__(
            hass, _LOGGER, name=DOMAIN, update_interval=SCAN_INTERVAL, update_method=self._async_update_data
        )

    def set_location(self, latitude: float, longitude: float):
        self.latitude = latitude
        self.longitude = longitude
        _LOGGER.info("new lat/lon. %s / %s", self.latitude, self.longitude)

    async def _async_update_data(self):
        """Get the latest data from OpenWeatherMap."""
        _LOGGER.info("Updating data")
        try:
            obs = await self.hass.async_add_executor_job(
                self.owmmgr.one_call, self.latitude, self.longitude
            )
        except exceptions.ParseAPIResponseError:
            _LOGGER.error("Error while connecting to OpenWeatherMap")
            obs = None
        if obs is None:
            _LOGGER.warning("Failed to fetch fresh data from OWM")
            return self.data

        self.data = obs
        return self.data

